---
layout: handbook-page-toc
title: "Account Planning"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 

An Account Plan is a single document that contains important details about a new prospect or existing customer, including information about their decision-making process, the companies you’re competing with, and your overall strategy to win them over (and retain them). This includes a 6, 12, and 18 month plan, with an emphasis on the next 6 months.

Think about account planning as if you were the CEO of your own territory. If you were a CEO, what information would you need to give banks, investors, or employees to demonstrate that you have a viable business plan that you can execute? An account plan is a business plan that helps you analyze and execute on your business.  

## Account Planning vs. Success Planning 

While the account plan focuses on the account team's strategy to win and retain customers, success planning clearly documents what and how GitLab will deliver value throughout the customer lifecycle to help the customer optimize their return on investment.

### Why Account Planning?

Account Planning helps *you* elevate opportunity-driven conversations into value-based conversations that focus on the customer's [value drivers](/handbook/sales/command-of-the-message/#customer-value-drivers).  Good account plans help you focus on what matters to drive increased share of wallet and predicitable growth. Remember this is more about creating a strategic plan for customer growth and retention, rather than creating a report for management.

An account plan can help:
* See what is going on across the business and determine which opportunities provide the biggest return on your team’s efforts
* Determine if your team has the resources to land/ expand this account
* Set your goals based on the opportunities and your resources to land the account
* Turn your goals into strategic objectives
* Determine what engagement approaches will you use to achieve the strategic objectives
* Assess the risks of failure and layout contingency plans


## Collaborate with the Account Team 

Account Plans should be thoughtful, collaborative, and cross-functional with contributions and input from the entire account team including Solutions Architects, Technical Account Managers, and Sales Leadership. 

## Account Plans are Living Documents

It's important to share progress with your account team and others (as appropriate) as things change to keep everyone informed and on the same page. 

A customer’s business and strategies are always subject to change. To stay up to date, share the account plan with the account team regularly to maintain a fresh understanding of the customer's needs. 

SALs should use QBRs and/or other recurring meetings such as SALSA/TAM calls) to review steps achieved thus far and set next steps or new objectives as needed. 

## Components of an Account Plan 
There are a few different components that are part of an account plan. 
* Account Profile 
* People Maps, including roles and reporting structure 
* Whitespace Mapping, including their current stages (whether GitLab, competitor, or status quo) and which stages should be targeted
* *Action Plan*

### Account Profile
Basic account details should already be entered into SalesForce. The purpose of the Account Profile section of the Account Plan is to understand and document the overall goal of the account.  What is the customer trying to achieve and why? What are they investing in?

Start at a high level, for example: 
1. What are the Key Drivers of Change such as Macro Environmental Issues like their Market conditions , competitive strategy, regulatory and compliance issues?
1. How does this impact their business strategy, what are they trying to achieve e.g.
    - Increased revenues - e.g new product/market (innovation)
    - Lowering costs
    - Process improvements
    - Customer satisfaction /NPS scores / Competitive pressures
    - Reduction of risk

How does any or all of that flow into their their digital strategy and most importantly how and where do we fit in - come back to that in the whitespace mapping? 

### Org Structure/ Relationships
Mapping the org allows us to analyze where we are strong and where we have vulnerabilities which will drive some of our actions later in the plan. Creating a map can help you think through how each part of the organization engages with other parts of the business, and their potential to influence other lines of business. 

If the account plan is with an existing customer you can track relationship health and trends to help with identifying a new point of entry. Net Promoter Score (NPS) helps determine if the customer is open to new opportunities.

Once you have identified the opportunity, think about who you need to align with. Ask yourself:
- What do you need to do to identify key roles and names? 
- Who is a champion, potential champion, detractor?
- What is their level of influence  and who are they aligned with?

### White Space Mapping
This is the part of the account plan where the account teams works through the "Why?", "What?", and how we fit with our customer or prospect.  Here we identify the opportunity path and our potential to be successful.  The best way to approach white space mapping by using the value framework in Command of Message and building a mantra that defines our customer value proposition.

You should think about the following:
- What is the current state, the negative consequences associated which failure to change and who is most affected
- What is the desired future state, the business benefits and the opportunity cost or doing nothing or something else
- What is the MVP to get there - here we can expand a little flesh out what Land and Expand looks like, what size is the opportunity if we succeed. 
- Why GitLab - what are our advantages,challenges, risks?  Potential partners? 
- Proof points - what we need to win - C level engagements, activities, references, etc..

### Action Plan ###

*This is the most important part of the account plan!* 

First, identify actions to secure a decision for GitLab. The action plan areas should be closely aligned to the buyer's journey. The action plan should also assign tasks to account team members who will take ownership in executing these steps. Be sure to include information about how to leverage strengths, eliminate risks or obstacles, and how to respond. Use the action plan to foster a team spirit of urgency and accountability.

Lastly, one of the fundamental benefits of an account plan is to figure out  what you don’t know, and what you need to learn, so iterating frequently on the plan will help identify and address gaps. This will put you in a much better position to make decisions and take actions which will lead to success.

## Building the Account Plan 

### Step 1: Account Prioritization 
It is expected that all accounts will have an account plan, but you do not need to create an account plan for all of your accounts at the same time. Ideally, you will focus on building up to three account plans at one time. The priority is thoughtful and strategic account plans versus the number of account plans created.


### Step 2: Understand the Customer’s Motivation
Always start with the Customer's "Why?", and get clarity on the customer’s strategies, goals, and objectives. Researching your customer is a key element to this step. 

### Step 3: Gather Data and Insights 
A good place to start is by looking at the customer's total addressable market (TAM). Review data available for the account including buying history/trends, industry influences, and partner interactions. Leverage partners, account-based marketing (ABM), and your account team to create a complete picture of the account.  

### Step 4: Start Building
Account Planning needs a short and a long term ‘vision’ for the account that both parties agree to work towards. A good place to start is with a 6 month plan. You can start by asking the customer: “Where do you want to be in the next year as it pertains to your DevSecOps maturity and implementation?” Take that answer and continue to ask the question in different ways to drive to clarity with the customer. Sometimes we might have to push the customer to think about things is a fresh way. 

Take that answer and make it the first point of the plan, then work back from there.  

#### Samples
The one year vision:
**The Customer says...“In the next year, the business will be …”**
* “To get there, the business will need to…”
* “Software development will play the role of…”
* “Our software teams will need to be able to…”
* “Our use of GitLab to support this will be…”
* “To get to that stage with GitLab, we will need to do…”
* “How we do that, is something … will need to own on our side”

Build out a plan of required capabilities, prioritise them, and then order them.
Some examples: 
* “In the next year, the business will be operating in 6 new countries, with 2 new product lines.  
* To get there the business will need to hire new talent, we will need to build the products, we will need to open 6 new regional entities.  We will also need to ensure that we are not over-extending the core profit making part of the business.  
* Software development’s role will be: 1) the products will be either fully or partially digital; 2) to launch the new product lines, in the new markets we wish to enter, will require new technical expertise in… AI/robotics/VR etc; 3) dev will need to ensure that the core cash-cow products at least keep their current margins while reducing cost.  


### Step 5: Reviewing and Iterate  
Account Plans are living documents and are never truly complete.  

Once you build the initial plan ask yourself:
* How can you determine the validity of the plan?
* What can you measure?
* Have I verified my understanding with my champion?

If you have a Technical Account Manager for an exisiting customer, check in with them on their [Success Plan](https://about.gitlab.com/handbook/customer-success/tam/success-plans/#overview) and ask if they are planning any [Executive Business Reviews](https://about.gitlab.com/handbook/customer-success/tam/ebr/). These documents can help validate your Account Plan and determine if the vision is aligned with each customer's goals. 

When you're ready, share the plan with your manager and confirm next steps, resources needed, and review any information that is unclear. If appropriate, share the plan with partners to ensure alignment and commitment. 

Once you have an action plan in place, and action items for each member of the team, get started. Plans will continue to evolve and the account team will continue to iterate on them. 

### Step 6: Maintaining  
A quarterly cadence to review and update plans can help to ensure that plans reflect our current understanding of the account and keep the account team aligned on plan strategy and execution. Major changes such as personnel changes, mergers/aquisitions, and competitive threats may trigger immediate review and updates. 

Structured review sessions can help keep the account team aligned and keep leadership informed of changes involving customers. 
