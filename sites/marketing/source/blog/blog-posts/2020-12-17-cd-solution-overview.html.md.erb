---
title: "GitLab’s continuous delivery"
author: Cesar Saavedra
author_gitlab: csaavedra1
author_twitter: cesar_saavedr
categories: unfiltered
image_title: '/images/blogimages/cd-solution-overview/CD-continuous-nature-cover-880x586.jpg'
description: "Learn about how GitLab can help you make your releases safe, low risk, worry-free, consistent and repeatable"
tags: CI, CD, DevOps, demo
cta_button_text: 'Watch GitLab’s continuous delivery video!' # optional
cta_button_link: 'https://youtu.be/QArt7rqfbqk'
guest: false
ee_cta: false # required only if you do not want to display the EE-trial banner
install_cta: false # required only if you do not want to display the 'Install GitLab' banner
twitter_text: "" # optional;  If no text is provided it will use post's title.
postType: content marketing # i.e.: content marketing, product, corporate
merch_banner: merch_one
merch_sidebar: merch_one
---

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
This blog post is [Unfiltered](/handbook/marketing/blog/unfiltered/#legal-disclaimer)
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

Modernizing your existing release processes does not take place from one day to the next, rather it is a journey, where improvements can be made in a phased manner. Organizations are unique and their adoption of Continuous Delivery principles can take many forms and can follow different paths that are appropriate to them.

GitLab provides a solution, which includes Continuous Delivery capabilities based on best practices, that allows organizations to modernize their release processes at their own pace. No matter where you are in your adoption journey, GitLab can help you make your releases safe, low risk, worry-free, consistent and repeatable. Let’s go over some of the ways that GitLab can help in a three-step incremental approach to modernizing your release processes.

## Consolidate heterogeneous tools into a single platform

At this first phase, the following capabilities can help you rationalize your disparate tools into GitLab:

Milestones  are a way to track issues and merge requests created to achieve a broader goal in a certain period of time. They allow you to organize issues and merge requests into a cohesive group, with an optional start date and an optional due date.

![milestone](/images/blogimages/cd-solution-overview/milestone.png){: .shadow.medium.center.wrap-text}

Issues, where product problems or new features are described, and Merge Requests, where solutions are developed, are key inputs to the Release Planning process.
As components of the release, issues and MRs provide the auditability and tracking of application changes done by the collaboration of DevOps Engineers, System Administrators, and Developers. To track groups of issues with the same theme, you can create Epics. In the example below, an Epic has been created for all the UI-related issues in a project.

![epic](/images/blogimages/cd-solution-overview/epic.png){: .shadow.medium.center.wrap-text}

Iterations are a way to track issues over a period of time and help you track velocity and volatility metrics. Iterations can be used with milestones for tracking over different time periods. You can track your project's sprints through their detailed iterations pages, which include many progress metrics.

![iteration](/images/blogimages/cd-solution-overview/iteration.png){: .shadow.medium.center.wrap-text}

As you assemble Epics, Milestones, and Iterations, you can visually track the release progress via the Roadmaps page, which helps to streamline the release process.

![roadmap](/images/blogimages/cd-solution-overview/roadmap.png){: .shadow.medium.center.wrap-text}

GitLab offers many approval gates for your release. You can set a Deploy Freeze window to temporarily halt automated deployments to production. This prevents unintended production releases during a period of time to help reduce uncertainty and risk of unscheduled outages.

![freeze](/images/blogimages/cd-solution-overview/freeze.png){: .shadow.medium.center.wrap-text}

In addition, as part of the release approval gates, you can protect the production environment by specifying who is allowed to deploy to it. Specific role and responsibility assignments streamline the approval gates and release process.

![protected-env](/images/blogimages/cd-solution-overview/protected-env.png){: .shadow.medium.center.wrap-text}

At the appropriate time, you can create the release, which automatically generates the Release Evidence. This streamlined process helps you reduce your release cycle times.

![release-evidence](/images/blogimages/cd-solution-overview/release-evidence.png){: .shadow.medium.center.wrap-text}

## Implement continuous delivery

These capabilities will help you set up continuous delivery best practices. At this second phase, every change is automatically deployed to the User Acceptance Testing env/Staging (with a manual deployment to prod). In this scenario, there is no need for a deploy freeze, the release manager can cut a release from Staging at any point in time.

You can take advantage of GitLab Auto DevOps, which help you to automatically create the release pipeline and relieve you from manually spending time creating your own pipeline. As a part of Auto DevOps, you can automatically deploy to Staging and manually deploy to production as well as enable canary deployments. Auto DevOps, which is based on best practices, helps you streamline the release process.

![enable-auto-devops](/images/blogimages/cd-solution-overview/enable-auto-devops.png){: .shadow.medium.center.wrap-text}

The first job in Auto DevOps is the build job, as shown below:

![build-job](/images/blogimages/cd-solution-overview/build-job.png){: .shadow.medium.center.wrap-text}

The build job applies the appropriate build strategy to create a Docker image of the application and stores it in the built-in Docker Registry.

![container-registry](/images/blogimages/cd-solution-overview/container-registry.png){: .shadow.medium.center.wrap-text}

Faster and more reliable releases happen when you have build components, like Docker images, that are readily available to the release process in a uniform and consistent manner. GitLab also includes a built-in Package Registry that supports many packaging technologies.

![package-registry](/images/blogimages/cd-solution-overview/package-registry.png){: .shadow.medium.center.wrap-text}

You have the ability to visualize what specific features will go into production via Review Apps. As updates are made to the application via Merge Requests, they kick off Review Apps, which streamline the review process including the automatic creation and destruction of an ephemeral review environment, on which stakeholders can verify the updates to the application before they are merged to the main line. Review Apps help increase code quality reducing the risk of unexpected production outages.

![review-apps](/images/blogimages/cd-solution-overview/review-apps.png){: .shadow.medium.center.wrap-text}

Once the application has been built and passed many automated tests, checks and verifications, the Auto DevOps pipeline automatically stands up a Staging environment and deploys the application to it.

![staging-env](/images/blogimages/cd-solution-overview/staging-env.png){: .shadow.medium.center.wrap-text}

At this point, you can manually deploy the updated application as a Canary deployment to the production environment. By doing this, you ship features to only a portion of the pods fleet and watch their behavior as a percentage of the user base visits the temporarily deployed feature. If all works well, you can deploy the feature to production knowing that it won’t cause any problems. You can then proceed to start rolling out the Canary deployment to 50% of the production pods. Incremental rollouts lower the risk of production outages delivering a better user experience and customer satisfaction. Advanced deployment techniques, like Canary, Incremental, and Blue-Green also  improve development and delivery efficiency streamlining the release process.

![rollout](/images/blogimages/cd-solution-overview/rollout.png){: .shadow.medium.center.wrap-text}

To check the running application for integrity, you can click on the “Open live environment” button.

![live-env-button](/images/blogimages/cd-solution-overview/live-env-button.png){: .shadow.medium.center.wrap-text}

This will open up the application in a different browser tab. But what if you run into an application error, as shown below:

![app-error](/images/blogimages/cd-solution-overview/app-error.png){: .shadow.medium.center.wrap-text}

In this case, you could decide to perform a rollback.To do so, you need to drill down into the production environment page and identify the release that had been successfully running before you performed the last deployment. This page is an auditable sequence of changes that have been applied to the production environment. You can then start the rollback process with a single click of a button. Rollbacks speed up recovery of production in case of failures lowering outage times and leading to better customer satisfaction and user experience.

![rollback](/images/blogimages/cd-solution-overview/rollback.png){: .shadow.medium.center.wrap-text}

Pipelines usually run automatically. However, if you would like to schedule the execution of a pipeline once a day at midnight, for example, so that Staging can have the most recent version of the application on a daily basis, you can do this from the  CI/CD->Schedules. Scheduling pipelines can improve the efficiency of the development life cycle and release processes.

![pipeline-sched](/images/blogimages/cd-solution-overview/pipeline-sched.png){: .shadow.medium.center.wrap-text}

While the application is running in production, you can track how the release is performing and quickly identify and troubleshoot any production issues. There are a few ways you can do this. One way is to access the “Monitoring” feature for a specific environment to track system and application metrics, such as system and pod memory usage, and # of cores used. The monitoring tracking includes markers (small rocket icon) when updates were introduced to the environment, so that fluctuations in the metrics can be correlated to a specific update.

![monitoring](/images/blogimages/cd-solution-overview/monitoring.png){: .shadow.medium.center.wrap-text}

Monitoring reduces the time to identify, resolve and preempt production problems lowering the risk of unscheduled outages. It also provides an opportunity to do business activity monitoring and optimize cloud costs. Not only is this type of monitoring useful to release managers but also to DevOps Engineers, Application Operators and Platform Engineers.

Another way you can monitor the release is by creating alerts to detect out-of-range metrics, which are visible on the overall operations metrics dashboard as well as on each specific environment window. Alerts can also automatically trigger ChatOps and email messages to appropriate individuals or groups.

![alerts](/images/blogimages/cd-solution-overview/alerts.png){: .shadow.medium.center.wrap-text}

You can manage alerts from the Operations Alerts window, a single location from which you can assess and handle alerts, which may include the manual or automatic rollback of a release.

![alerts-window](/images/blogimages/cd-solution-overview/alerts-window.png){: .shadow.medium.center.wrap-text}

In addition, you can track and monitor the release progress through Value Stream Analytics, where you can check your project or group statistics over time and see how your team improves in the number of new issues, commits, deploys and deployment frequency. Value Stream Analytics is useful in order to quickly determine the velocity of a given project. It points to bottlenecks in the development process, enabling management to uncover, triage, and identify the root cause of slowdowns in the software development life cycle.

![value-stream](/images/blogimages/cd-solution-overview/value-stream.png){: .shadow.medium.center.wrap-text}

Lastly, another way for you to track and monitor the release is through Pipeline Analytics. Pipeline analytics shows the history of your pipeline successes and failures, as well as how long each pipeline ran to help you understand the health of your projects and their continuous delivery.

![pipeline-analytics](/images/blogimages/cd-solution-overview/pipeline-analytics.png){: .shadow.medium.center.wrap-text}

If you need to oversee more than one release, you can update your Operations dashboard by adding projects to it. Through this dashboard, you get a summary of each project’s operational health, including pipeline and alert status.

![ops-dashboard](/images/blogimages/cd-solution-overview/ops-dashboard.png){: .shadow.medium.center.wrap-text}

Similar to the Operations Dashboard, you can also access the Environments dashboard, which provides a cross-project environment-based view that lets you see the big picture of what is going on in each environment.

![env-dashboard](/images/blogimages/cd-solution-overview/env-dashboard.png){: .shadow.medium.center.wrap-text}

Or you can drill down into a specific environment to get all the updates that have been applied to it.

![prod-env-dashboard](/images/blogimages/cd-solution-overview/prod-env-dashboard.png){: .shadow.medium.center.wrap-text}

All these dashboards provide you with the Operations Insights that you need to understand how a release is performing in production and quickly identify and troubleshoot any production issues.

## Implement continuous deployment

At this third phase, the following capabilities can help you get set up for deploying updates directly to production. 

If you would like to introduce a feature to a segment of your end users in a controlled manner in production, you can create Feature Flags.  Feature flags help you reduce risk, allowing you to do controlled testing, and separate feature delivery from customer launch.

![feature-flag](/images/blogimages/cd-solution-overview/feature-flag.png){: .shadow.medium.center.wrap-text}

If you would like to identify who introduced a feature flag, you can open the project’s audit events dashboard to find out.

![events-dashboard](/images/blogimages/cd-solution-overview/events-dashboard.png){: .shadow.medium.center.wrap-text}

You can also check security and compliance related items of the project by going to the Security Dashboard.

![sec-dashboard](/images/blogimages/cd-solution-overview/sec-dashboard.png){: .shadow.medium.center.wrap-text}

These dashboards help you preempt out-of-compliance scenarios to avoid penalties. They also streamline audits, provide an opportunity to optimize cost, and lower risk of unscheduled production outages.

We have gone over how GitLab can help you make your releases safe, low risk, worry-free, consistent and repeatable. Whether you are just starting your journey into DevOps or in the midst of it, GitLab can help you at every step with capabilities built on DevOps and Continuous Delivery best practices.

If you’d like to see GitLab’s continuous delivery solution in action, watch this [video](https://youtu.be/QArt7rqfbqk).

For more information, visit [LEARN@GITLAB](https://about.gitlab.com/learn/).
